//
//  AppDelegate.h
//  nwwd
//
//  Created by 孙智 on 2017/6/16.
//  Copyright © 2017年 孙智. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

